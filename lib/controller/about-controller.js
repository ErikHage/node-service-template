const serviceService = require('../service/about-service');
const tryDecorator = require('../middleware/try-decorator');
const { statusCodes } = require('../helpers/constants');

const getServiceInfo = (req, res) => {
  const data = serviceService.getServiceInfo();
  res.status(statusCodes.OK).send(data);
};

module.exports = {
  getServiceInfo: tryDecorator(getServiceInfo),
};
