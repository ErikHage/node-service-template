const router = require('express').Router();

const serviceController = require('./controller/about-controller');

router.get('/service/about',
  serviceController.getServiceInfo);

module.exports = router;
