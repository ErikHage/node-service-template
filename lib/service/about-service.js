const packageJson = require('../../package.json');

const getServiceInfo = () => ({
  serviceName: packageJson.name,
  version: packageJson.version,
  description: packageJson.description,
  author: packageJson.author,
});

module.exports = {
  getServiceInfo,
};
